
import { of as observableOf, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from '../../@core/data/global.service';

@Injectable()
export class UserService {

  private user = {
    user: false,
    name: 'loading',
    picture: 'assets/images/user.png'
  };

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
  ) { }

  // getMenu(): Observable<any> {
  //   return observableOf(this.menu_item);
  // }

  getUser() : Observable<any> {
    this.updateUser();
    return observableOf(this.user);
  }

  updateUser(){
    if (localStorage.getItem('TF')) {
      this.http.get(this.globalService.api_baseurl + '/api/bo/account_profile?fcm_token=' + localStorage.getItem('TF'), {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(
        (res: any) => {
          if (res["message"] == "Success") {
            // this.menu_item = res['data'].roles_data
            // this.nbMenuService.addItems(this.menu_item);
            this.user.user = true
            this.user.name = res['data'].users_full_name
          }
        },
        _error => {
          this.globalService.goToLogin();
        }
      );
    } else {
      this.globalService.goToLogin();
    }
  }

  setUser(name, token, fcm_token) { 
    // debugger
    // this.menu_item = role
    this.user.user = true
    this.user.name = name
    localStorage.setItem('T', token);
    localStorage.setItem('TF', fcm_token);
  }
}
