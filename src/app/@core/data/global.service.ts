import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

  public api_baseurl = 'https://apistage-v2.meenistry.com';
  // public api_baseurl = 'http://192.168.136.151';
  public loading = false;
  public location = '';

  constructor(
    private router: Router
  ) {
    this.loading = false;
   }

  goToLogin(){
    this.router.navigate(['login']);
  }
  startLoading(){
    this.loading = true;
  }
  stopLoading(){
    this.loading = false;
  }
}
