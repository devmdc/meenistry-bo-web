import { NgModule } from '@angular/core';
import { NbDialogModule, NbWindowModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ClientRoutingModule, routedComponents } from './client-routing.module';
import { VenueAddModalComponent } from './venue/add-modal/add-modal.component';
import { VenueDetailModalComponent } from './venue/detail-modal/detail-modal.component';
import { SetupStyleAddModalComponent } from './setup_style/add-modal/add-modal.component';
import { SetupStyleDetailModalComponent } from './setup_style/detail-modal/detail-modal.component';
import { EventTypeAddModalComponent } from './event_type/add-modal/add-modal.component';
import { EventTypeDetailModalComponent } from './event_type/detail-modal/detail-modal.component';
import { FacilitiesAddModalComponent } from './facilities/add-modal/add-modal.component';
import { FacilitiesDetailModalComponent } from './facilities/detail-modal/detail-modal.component';

const COMPONENTS = [
  VenueAddModalComponent,
  VenueDetailModalComponent,
  SetupStyleAddModalComponent,
  SetupStyleDetailModalComponent,
  EventTypeAddModalComponent,
  EventTypeDetailModalComponent,
  FacilitiesAddModalComponent,
  FacilitiesDetailModalComponent
];
const ENTRY_COMPONENTS = [
  VenueAddModalComponent,
  VenueDetailModalComponent,
  SetupStyleAddModalComponent,
  SetupStyleDetailModalComponent,
  EventTypeAddModalComponent,
  EventTypeDetailModalComponent,
  FacilitiesAddModalComponent,
  FacilitiesDetailModalComponent
];
const MODULES = [
  ThemeModule,
  NbDialogModule.forChild(),
  NbWindowModule.forChild(),
];

@NgModule({
  imports: [
    ClientRoutingModule,
    ...MODULES,
  ],
  declarations: [
    ...routedComponents,
    ...COMPONENTS,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class ClientModule { }
