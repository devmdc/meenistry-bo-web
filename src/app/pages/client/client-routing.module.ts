import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientComponent } from './client.component';
import { VenueComponent } from './venue/venue.component';
import { SetupStyleComponent } from './setup_style/setup_style.component';
import { EventTypeComponent } from './event_type/event_type.component';
import { FacilitiesComponent } from './facilities/facilities.component';

const routes: Routes = [{
  path: '',
  component: ClientComponent,
  children: [{
    path: 'venue',
    component: VenueComponent,
  },
  {
    path: 'setup_style',
    component: SetupStyleComponent,
  },
  {
    path: 'event_type',
    component: EventTypeComponent,
  },
  {
    path: 'facilities',
    component: FacilitiesComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientRoutingModule { }

export const routedComponents = [
  ClientComponent,
  VenueComponent,
  SetupStyleComponent,
  EventTypeComponent,
  FacilitiesComponent,
];
