import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-detail-modal',
  templateUrl: 'detail-modal.component.html',
  styleUrls: ['detail-modal.component.scss'],
})
export class FacilitiesDetailModalComponent implements OnInit {

  loading = true
  error_message = ''
  @Input() id_facilities;

  dialog_action;

  name_facilities = '';

  data_detail = [];
  data_edit = {};

  logoURL;
  imageURL;
  brochureURL;
  logo_file: File;
  image_file: File;
  brochure_file: File;
  logo_url;
  image_url;
  brochure_url;

  data_message = 'loading'

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<FacilitiesDetailModalComponent>
  ) {
    this.dialog_action = "detail";
  }

  ngOnInit() {
    this.loading = true
    this.load_facilities();
  }

  dismiss() {
    this.ref.close();
  }

  load_facilities() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/client/facilities_detail?facility_id=' + this.id_facilities, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.name_facilities = res['data'].name;

          this.data_detail['name'] = res['data'].name;
          this.data_detail['image'] = res['data'].image;

          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.loading = false
      }
    );
  }

  detail() {
    this.dialog_action = 'detail';
    this.load_facilities();
  }

  edit() {
    this.data_edit['name'] = this.data_detail['name'];
    this.imageURL = this.data_detail['image'];

    this.dialog_action = 'edit';
  }

  go_revert() {
    this.loading = true
    this.load_facilities();
    this.edit();
    this.loading = false
  }

  go_edit() {
    this.dialog_action = 'edit'
    this.error_message = null
    this.loading = true
    if (this.data_edit['name'] == null || this.data_edit['name'] === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    }
    let data = {
      "facility_id": this.id_facilities,
      "name": this.data_edit['name']
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/client/facilities_edit',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }
    ).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_message = res['message'];
          this.dismiss();
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },

      _error => {
        this.loading = false
        if (_error.error.message) {
          this.error_message = _error.error.message;
          return;
        } else {
          this.error_message = 'please check your network!';
          return;
        }
      }
    );;
  }


  delete() {
    this.dialog_action = 'delete';
  }

  go_delete() {
    this.loading = true;
    let data = {
      "facility_id": this.id_facilities
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/client/facilities_delete',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          if (res['message'] == 'Success') {
            this.data_message = res['message'];
            this.dismiss();
          } else {
            this.data_message = res['message'];
          }
          this.loading = false
        },
        _error => {
          if (_error.error.message) {
            this.data_message = _error.error.message;
          } else {
            this.data_message = 'please check your network!';
          }
          this.loading = false
        }
      );
  }

}
