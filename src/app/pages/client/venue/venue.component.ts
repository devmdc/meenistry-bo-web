import { Component, OnInit, TemplateRef   } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbMenuService, NbDialogService } from '@nebular/theme';

import { GlobalService } from '../../../@core/data/global.service';

import { VenueAddModalComponent } from './add-modal/add-modal.component';
import { VenueDetailModalComponent } from './detail-modal/detail-modal.component';

@Component({
  selector: 'ngx-venue',
  styleUrls: ['./venue.component.scss'],
  templateUrl: './venue.component.html',
})
export class VenueComponent implements OnInit {

  data_start = 1;
  data_end = 10;
  data_total = 10;
  page_current = 1;
  page_total = 1;

  data_venue = [];
  data_message = 'loading'

  constructor(

    private http: HttpClient,
    private dialogService: NbDialogService,
    private menuService: NbMenuService,
    private globalService: GlobalService
  ) { }

  ngOnInit() {
    this.globalService.startLoading();
    this.globalService.location = 'Client - Venue'
    this.load_venue();
  }

  load_venue() {
    this.globalService.startLoading()
    this.http.get(this.globalService.api_baseurl + '/api/bo/client/venue_list?page='+this.page_current+'&limit=20', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_venue = res['data'];
          this.data_start = res['meta'].current_data_start;
          this.data_end = res['meta'].current_data_end;
          this.data_total = res['meta'].total_data;
          this.page_current = res['meta'].current_page;
          this.page_total = res['meta'].total_page;
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
  }

  load_detail_venue(id) {
    this.dialogService.open(VenueDetailModalComponent, {
      context: {
        id_venue: id
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_venue() }
    );
  }

  load_add_venue() {
    this.dialogService.open(VenueAddModalComponent, {
      context: {
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_venue() }
    );
  }
}
