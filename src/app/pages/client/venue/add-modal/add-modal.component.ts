import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-add-modal',
  templateUrl: 'add-modal.component.html',
  styleUrls: ['add-modal.component.scss'],
})
export class VenueAddModalComponent implements OnInit {

  // @Input() id_venue;
  loading = true
  error_message = '';
  dialog_action = 'add'
  name;
  propertytype;
  bisnistype;
  yearbuilt;
  lastrenovate;
  description;
  overview;
  logo;
  logoURL;
  image;
  imageURL;
  brochure;
  brochureURL;
  logo_file : File;
  image_file : File;
  brochure_file : File;
  logo_url;
  image_url;
  brochure_url;
  country;
  province;
  city;
  address1;
  address2;
  postalcode;
  longitudelatitude;
  phone;
  email;
  website;
  facebook;
  twitter;
  instagram;
  linkedin;
  currency;
  outsidecatering = false;
  corkage;
  pb1;
  servicecharge;
  open;
  close;
  fullname;
  user_email;
  user_phone;
  user_whatsapp;

  data_message = 'loading';

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<VenueAddModalComponent>
  ) { }

  ngOnInit() {
    this.loading = true
    this.loading = false
  }

  clear() {
    this.name = null;
    this.propertytype = null;
    this.bisnistype = null;
    this.yearbuilt = null;
    this.lastrenovate = null;
    this.description = null;
    this.overview = null;
    this.logo = null;
    this.image = null;
    this.brochure = null;
    this.logoURL = null;
    this.imageURL = null;
    this.brochureURL = null;
    this.logo_url = null;
    this.image_url = null;
    this.brochure_url = null;
    this.logo_file = null;
    this.image_file = null;
    this.brochure_file = null;
    this.country = null;
    this.province = null;
    this.city = null;
    this.address1 = null;
    this.address2 = null;
    this.postalcode = null;
    this.longitudelatitude = null;
    this.phone = null;
    this.email = null;
    this.website = null;
    this.facebook = null;
    this.twitter = null;
    this.instagram = null;
    this.linkedin = null;
    this.currency = null;
    this.outsidecatering = false;
    this.corkage = null;
    this.pb1 = null;
    this.servicecharge = null;
    this.open = null;
    this.close = null;
    this.fullname = null;
    this.user_email = null;
    this.user_phone = null;
    this.user_whatsapp = null;
  }

  dismiss() {
    this.ref.close();
  }

  outsidecatering_change() {
    if (this.outsidecatering == false) {
      this.corkage = 0
    }
  }

  open_change(){
    var temp_open = +this.open
    var temp_close = +this.close
    if (temp_open > 23) {
      this.open = 23;
    } else if (temp_open < 0) {
      this.open = 0;
    }
    if (temp_close <= temp_open) {
      this.close = temp_open + 1;
    }  
  }

  close_change(){
    var temp_open = +this.open
    var temp_close = +this.close
    if (temp_close > 24) {
      this.close = 24;
    } else if (temp_close <= temp_open) {
      this.close = temp_open + 1;
    } 
  }

  changeLogo(event) {
    if (event.target.files.length === 0) return; 
    this.logo_file = event.target.files[0]; 
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.logoURL = reader.result; 
    }
  }

  changeImage(event){    
    if (event.target.files.length === 0) return; 
    this.image_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.imageURL = reader.result; 
    }
  }

  changeBrochure(event){
    if (event.target.files.length === 0) return; 
    this.brochure_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.brochureURL = reader.result; 
    }
  }

  add() {    
    this.dialog_action = 'add'
    this.error_message = null
    this.loading = true
    if (this.name == null || this.name === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if(this.propertytype == null || this.propertytype === '') {
      this.error_message = 'Property type required!';
      this.loading = false
      return;
    } else if(this.bisnistype == null || this.bisnistype === '') {
      this.error_message = 'Bisnis type required!';
      this.loading = false
      return;
    } else if(this.yearbuilt == null || this.yearbuilt === '' || this.yearbuilt <= 0) {
      this.error_message = 'Year built required!';
      this.loading = false
      return;
    } else if(this.lastrenovate == null || this.lastrenovate === '' || this.lastrenovate <= 0) {
      this.error_message = 'Last renovate required!';
      this.loading = false
      return;
    } else if(this.description == null || this.description === '') {
      this.error_message = 'Description required!';
      this.loading = false
      return;
    } else if(this.overview == null || this.overview === '') {
      this.error_message = 'Overview required!';
      this.loading = false
      return;
    } else if(this.logo == null || this.logo === '') {
      this.error_message = 'Logo required!';
      this.loading = false
      return;
    } else if(this.image == null || this.image === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    } else if(this.brochure == null || this.brochure === '') {
      this.error_message = 'Brochure required!';
      this.loading = false
      return;
    } else if(this.country == null || this.country === '') {
      this.error_message = 'Country required!';
      this.loading = false
      return;
    } else if(this.province == null || this.province === '') {
      this.error_message = 'Province required!';
      this.loading = false
      return;
    } else if(this.city == null || this.city === '') {
      this.error_message = 'City required!';
      this.loading = false
      return;
    } else if(this.address1 == null || this.address1 === '') {
      this.error_message = 'Address 1 required!';
      this.loading = false
      return;
    } else if(this.address2 == null || this.address2 === '') {
      this.error_message = 'Address 2 required!';
      this.loading = false
      return;
    } else if(this.postalcode == null || this.postalcode === '') {
      this.error_message = 'Postal code required!';
      this.loading = false
      return;
    } else if(this.longitudelatitude == null || this.longitudelatitude === '') {
      this.error_message = 'Longitude latitude required!';
      this.loading = false
      return;
    } else if(this.phone == null || this.phone === '' || this.phone.length <= 4) {
      this.error_message = 'Phone required! min length is 5';
      this.loading = false
      return;
    } else if(this.email == null || this.email === '') {
      this.error_message = 'Email required!';
      this.loading = false
      return;
    } else if(this.website == null || this.website === '') {
      this.error_message = 'Website required!';
      this.loading = false
      return;
    } else if(this.facebook == null || this.facebook === '') {
      this.error_message = 'Facebook required!';
      this.loading = false
      return;
    } else if(this.twitter == null || this.twitter === '') {
      this.error_message = 'Twitter required!';
      this.loading = false
      return;
    } else if(this.instagram == null || this.instagram === '') {
      this.error_message = 'Instagram required!';
      this.loading = false
      return;
    } else if(this.linkedin == null || this.linkedin === '') {
      this.error_message = 'Linkedin required!';
      this.loading = false
      return;
    } else if(this.currency == null || this.currency === '' || this.currency.length != 3) {
      this.error_message = 'Currency required! length must 3';
      this.loading = false
      return;
    } else if((this.outsidecatering && this.corkage == null) || (this.outsidecatering && this.corkage === '') || (this.outsidecatering && this.corkage <= 0) || (this.outsidecatering && this.corkage > 100)) {
      this.error_message = 'Corkage required! min 1 and max 100';
      this.loading = false
      return;
    } else if(this.pb1 == null || this.pb1 === '' || this.pb1 < 0 || this.pb1 > 100) {
      this.error_message = 'PB 1 required! min 0 and max 100';
      this.loading = false
      return;
    } else if(this.servicecharge == null || this.servicecharge === '' || this.servicecharge < 0 || this.servicecharge > 100) {
      this.error_message = 'Service charge required! min 0 and max 100';
      this.loading = false
      return;
    } else if(this.open == null || this.open === '' || this.open < 0 || this.open > 23) {
      this.error_message = 'Open required! min 0 and max 23';
      this.loading = false
      return;
    } else if(this.close == null || this.close === '' || this.close <= this.open  || this.close > 24) {
      var temp_open = +this.open
      temp_open = temp_open + 1
      this.error_message = 'Close required! min ' + temp_open + ' and max 24';
      this.loading = false
      return;
    } else if(this.fullname == null || this.fullname === '') {
      this.error_message = 'Full name required!';
      this.loading = false
      return;
    } else if(this.user_email == null || this.user_email === '') {
      this.error_message = 'Email user required!';
      this.loading = false
      return;
    } else if(this.user_phone == null || this.user_phone === '' || this.user_phone.length <= 4) {
      this.error_message = 'Phone user required! min length is 5';
      this.loading = false
      return;
    } else if(this.user_whatsapp == null || this.user_whatsapp === '' || this.user_whatsapp.length <= 4) {
      this.error_message = 'Whatsapp user required! min length is 5';
      this.loading = false
      return;
    }

    var re = /(?:\.([^.]+))?$/;

    var logo_ext = re.exec(this.logo)[1];
    const logo_blob = this.logo_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", logo_blob, {
      headers: {
        "Content-Type": "image/" + logo_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.logo_url = res["data"].uploadfiles_fileurl
        var image_ext = re.exec(this.image)[1];
        const image_blob = this.image_file as Blob;
        return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
          headers: {
            "Content-Type": "image/" + image_ext,
            Authorization: "Bearer " + localStorage.getItem('T')
          }
        }).subscribe(

          (res: any) => {
            this.image_url = res["data"].uploadfiles_fileurl
            var brochure_ext = re.exec(this.brochure)[1];
            const image_blob = this.brochure_file as Blob;
            return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
              headers: {
                "Content-Type": "image/" + brochure_ext,
                Authorization: "Bearer " + localStorage.getItem('T')
              }
            }).subscribe(

              (res: any) => {
                this.brochure_url = res["data"].uploadfiles_fileurl
                let data = {
                  "venues_name": this.name,
                  "venues_propertytype": this.propertytype,
                  "venues_bisnistype": this.bisnistype,
                  "venues_outsidecatering": this.outsidecatering,
                  "venues_corkage": this.corkage,
                  "venues_pb1": this.pb1,
                  "venues_servicecharge": this.servicecharge,
                  "venues_currency": this.currency,
                  "venues_logo": this.logo_url,
                  "venues_image": this.image_url,
                  "venues_email": this.email,
                  "venues_description": this.description,
                  "venues_country": this.country,
                  "venues_city": this.city,
                  "venues_address1": this.address1,
                  "venues_address2": this.address2,
                  "venues_province": this.province,
                  "venues_postalcode": this.postalcode,
                  "venues_longlat": this.longitudelatitude,
                  "venues_phone": this.phone,
                  "venues_open": this.open,
                  "venues_close": this.close,
                  "venues_fullday": 8,
                  "venues_halfday": 4,
                  "venues_overview": this.overview,
                  "venues_website": this.website,
                  "venues_brochure": this.brochure_url,
                  "venues_facebook": this.facebook,
                  "venues_twitter": this.twitter,
                  "venues_instagram": this.instagram,
                  "venues_googleplus": "",
                  "venues_linkedin": this.linkedin, 
                  "venues_yearbuilt": this.yearbuilt,
                  "venues_lastrenovate": this.lastrenovate,
                  "users_full_name": this.fullname,
                  "users_email": this.user_email,
                  "users_phone": this.user_phone,
                  "users_whatsapp": this.user_whatsapp
                }
                this.http.post(this.globalService.api_baseurl + '/api/bo/client/venue_add',
                  data, {
                    headers: {
                      "Content-Type": "application/json",
                      'Authorization': 'Bearer ' + localStorage.getItem('T')
                    }
                  }).subscribe(
            
                    (res: any) => {
                      this.loading = false;
                      this.dismiss();
                      return;
                    },
            
                    _error => {
                      this.loading = false
                      if (_error.error.message) {
                        this.error_message = _error.error.message;
                        return;
                      } else {
                        this.error_message = 'please check your network!';
                        return;
                      }
                    }
                  );
              },

              _error => {
                this.loading = false
                if (_error.error.message) {
                  this.error_message = _error.error.message;
                  return;
                } else {
                  this.error_message = 'please check your network!';
                  return;
                }
              }
            );
          },

          _error => {
            this.loading = false
            if (_error.error.message) {
              this.error_message = _error.error.message;
              return;
            } else {
              this.error_message = 'please check your network!';
              return;
            }
          }
        );
      },

      _error => {
        this.loading = false
        if (_error.error.message) {
          this.error_message = _error.error.message;
          return;
        } else {
          this.error_message = 'please check your network!';
          return;
        }
      }
    );
  }
}
