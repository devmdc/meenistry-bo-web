import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-detail-modal',
  templateUrl: 'detail-modal.component.html',
  styleUrls: ['detail-modal.component.scss'],
})
export class VenueDetailModalComponent implements OnInit {

  loading = true
  error_message = ''
  @Input() id_venue;
  
  dialog_action;

  name_venue = '';

  data_detail = [];
  data_edit = {};
  
  logoURL;
  imageURL;
  brochureURL;
  logo_file : File;
  image_file : File;
  brochure_file : File;
  logo_url;
  image_url;
  brochure_url;

  data_message = 'loading'

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<VenueDetailModalComponent>
    ) {
      this.dialog_action = "detail";
    }

  ngOnInit() {
    this.loading = true
    this.load_venue();
  }

  dismiss() {
    this.ref.close();
  }

  load_venue() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/client/venue_detail?venue_id='+this.id_venue, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.name_venue = res['data'].venues_name;

          this.data_detail['name'] = res['data'].venues_name;
          this.data_detail['propertytype'] = res['data'].venues_propertytype;
          this.data_detail['bisnistype'] = res['data'].venues_bisnistype;
          this.data_detail['yearbuilt'] = res['data'].venues_yearbuilt;
          this.data_detail['lastrenovate'] = res['data'].venues_lastrenovate;
          this.data_detail['description'] = res['data'].venues_description;
          this.data_detail['overview'] = res['data'].venues_overview;

          this.data_detail['logo'] = res['data'].venues_logo;
          this.data_detail['image'] = res['data'].venues_image;
          this.data_detail['brochure'] = res['data'].venues_brochure;

          this.data_detail['country'] = res['data'].venues_country;
          this.data_detail['province'] = res['data'].venues_province;
          this.data_detail['city'] = res['data'].venues_city;
          this.data_detail['address1'] = res['data'].venues_address1;
          this.data_detail['address2'] = res['data'].venues_address2;
          this.data_detail['postalcode'] = res['data'].venues_postalcode;
          this.data_detail['longitudelatitude'] = res['data'].venues_longlat;

          this.data_detail['phone'] = res['data'].venues_phone;
          this.data_detail['email'] = res['data'].venues_email;
          this.data_detail['website'] = res['data'].venues_website;
          this.data_detail['facebook'] = res['data'].venues_facebook;
          this.data_detail['twitter'] = res['data'].venues_twitter;
          this.data_detail['instagram'] = res['data'].venues_instagram;
          this.data_detail['linkedin'] = res['data'].venues_linkedin; 
          this.data_detail['googleplus'] = res['data'].venues_googleplus;

          this.data_detail['currency'] = res['data'].venues_currency;
          this.data_detail['outsidecatering'] = res['data'].venues_outsidecatering;
          this.data_detail['corkage'] = res['data'].venues_corkage;
          this.data_detail['pb1'] = res['data'].venues_pb1;
          this.data_detail['servicecharge'] = res['data'].venues_servicecharge;
          this.data_detail['open'] = res['data'].venues_open;
          this.data_detail['close'] = res['data'].venues_close;
          this.data_detail['fullday'] = res['data'].venues_fullday;
          this.data_detail['halfday'] = res['data'].venues_halfday;  
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.loading = false
      }
    );
  }

  detail(){
    this.dialog_action = 'detail';
    this.load_venue();
  } 

  edit(){
    
    this.data_edit['name'] = this.data_detail['name'];
    this.data_edit['propertytype'] = this.data_detail['propertytype'];
    this.data_edit['bisnistype'] = this.data_detail['bisnistype'];
    this.data_edit['yearbuilt'] = this.data_detail['yearbuilt'];
    this.data_edit['lastrenovate'] = this.data_detail['lastrenovate'];
    this.data_edit['description'] = this.data_detail['description'];
    this.data_edit['overview'] = this.data_detail['overview'];

    this.logoURL = this.data_detail['logo'];
    this.imageURL = this.data_detail['image'];
    this.brochureURL = this.data_detail['brochure'];

    this.data_edit['country'] = this.data_detail['country'];
    this.data_edit['province'] = this.data_detail['province'];
    this.data_edit['city'] = this.data_detail['city'];
    this.data_edit['address1'] = this.data_detail['address1'];
    this.data_edit['address2'] = this.data_detail['address2'];
    this.data_edit['postalcode'] = this.data_detail['postalcode'];
    this.data_edit['longitudelatitude'] = this.data_detail['longitudelatitude'];

    this.data_edit['phone'] = this.data_detail['phone'];
    this.data_edit['email'] = this.data_detail['email'];
    this.data_edit['website'] = this.data_detail['website'];
    this.data_edit['facebook'] = this.data_detail['facebook'];
    this.data_edit['twitter'] = this.data_detail['twitter'];
    this.data_edit['instagram'] = this.data_detail['instagram'];
    this.data_edit['linkedin'] = this.data_detail['linkedin'];
    this.data_edit['googleplus'] = this.data_detail['googleplus'];

    this.data_edit['currency'] = this.data_detail['currency'];
    this.data_edit['outsidecatering'] = this.data_detail['outsidecatering'];
    this.data_edit['corkage'] = this.data_detail['corkage'];
    this.data_edit['pb1'] = this.data_detail['pb1'];
    this.data_edit['servicecharge'] = this.data_detail['servicecharge'];
    this.data_edit['open'] = this.data_detail['open'];
    this.data_edit['close'] = this.data_detail['close'];
    this.data_edit['fullday'] = this.data_detail['fullday'];
    this.data_edit['halfday'] = this.data_detail['halfday'];

    this.dialog_action = 'edit';
  }

  outsidecatering_change() {
    if (this.data_edit['outsidecatering'] == false) {
      this.data_edit['corkage'] = 0
    }
  }

  open_change(){
    var temp_open = +this.data_edit['open']
    var temp_close = +this.data_edit['close']
    if (temp_open > 23) {
      this.data_edit['open'] = 23;
    } else if (temp_open < 0) {
      this.data_edit['open'] = 0;
    }
    if (temp_close <= temp_open) {
      this.data_edit['close'] = temp_open + 1;
    }  
  }

  close_change(){
    var temp_open = +this.data_edit['open']
    var temp_close = +this.data_edit['close']
    if (temp_close > 24) {
      this.data_edit['close'] = 24;
    } else if (temp_close <= temp_open) {
      this.data_edit['close'] = temp_open + 1;
    } 
  }

  changeLogo(event) {
    if (event.target.files.length === 0) return; 
    this.logo_file = event.target.files[0]; 
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.logoURL = reader.result; 
    }
  }

  changeImage(event){    
    if (event.target.files.length === 0) return; 
    this.image_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.imageURL = reader.result; 
    }
  }

  changeBrochure(event){
    if (event.target.files.length === 0) return; 
    this.brochure_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.brochureURL = reader.result; 
    }
  }

  go_edit() {    
    this.dialog_action = 'edit'
    this.error_message = null
    this.loading = true
    if (this.data_edit['name'] == null || this.data_edit['name'] === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if(this.data_edit['propertytype'] == null || this.data_edit['propertytype'] === '') {
      this.error_message = 'Property type required!';
      this.loading = false
      return;
    } else if(this.data_edit['bisnistype'] == null || this.data_edit['bisnistype'] === '') {
      this.error_message = 'Bisnis type required!';
      this.loading = false
      return;
    } else if(this.data_edit['yearbuilt'] == null || this.data_edit['yearbuilt'] === '' || this.data_edit['yearbuilt'] <= 0) {
      this.error_message = 'Year built required!';
      this.loading = false
      return;
    } else if(this.data_edit['lastrenovate'] == null || this.data_edit['lastrenovate'] === '' || this.data_edit['lastrenovate'] <= 0) {
      this.error_message = 'Last renovate required!';
      this.loading = false
      return;
    } else if(this.data_edit['description'] == null || this.data_edit['description'] === '') {
      this.error_message = 'Description required!';
      this.loading = false
      return;
    } else if(this.data_edit['overview'] == null || this.data_edit['overview'] === '') {
      this.error_message = 'Overview required!';
      this.loading = false
      return;
    } else if((this.data_edit['logo'] == null && (this.logoURL == null || this.logoURL === '')) || this.data_edit['logo'] === '') {
      this.error_message = 'Logo required!';
      this.loading = false
      return;
    } else if((this.data_edit['image'] == null && (this.imageURL == null || this.imageURL === '')) || this.data_edit['image'] === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    } else if((this.data_edit['brochure'] == null && (this.brochureURL == null || this.brochureURL === '')) || this.data_edit['brochure'] === '') {
      this.error_message = 'Brochure required!';
      this.loading = false
      return;
    } else if(this.data_edit['country'] == null || this.data_edit['country'] === '') {
      this.error_message = 'Country required!';
      this.loading = false
      return;
    } else if(this.data_edit['province'] == null || this.data_edit['province'] === '') {
      this.error_message = 'Province required!';
      this.loading = false
      return;
    } else if(this.data_edit['city'] == null || this.data_edit['city'] === '') {
      this.error_message = 'City required!';
      this.loading = false
      return;
    } else if(this.data_edit['address1'] == null || this.data_edit['address1'] === '') {
      this.error_message = 'Address 1 required!';
      this.loading = false
      return;
    } else if(this.data_edit['address2'] == null || this.data_edit['address2'] === '') {
      this.error_message = 'Address 2 required!';
      this.loading = false
      return;
    } else if(this.data_edit['postalcode'] == null || this.data_edit['postalcode'] === '') {
      this.error_message = 'Postal code required!';
      this.loading = false
      return;
    } else if(this.data_edit['longitudelatitude'] == null || this.data_edit['longitudelatitude'] === '') {
      this.error_message = 'Longitude latitude required!';
      this.loading = false
      return;
    } else if(this.data_edit['phone'] == null || this.data_edit['phone'] === '' || this.data_edit['phone'].length <= 4) {
      this.error_message = 'Phone required! min length is 5';
      this.loading = false
      return;
    } else if(this.data_edit['email'] == null || this.data_edit['email'] === '') {
      this.error_message = 'Email required!';
      this.loading = false
      return;
    } else if(this.data_edit['website'] == null || this.data_edit['website'] === '') {
      this.error_message = 'Website required!';
      this.loading = false
      return;
    } else if(this.data_edit['facebook'] == null || this.data_edit['facebook'] === '') {
      this.error_message = 'Facebook required!';
      this.loading = false
      return;
    } else if(this.data_edit['twitter'] == null || this.data_edit['twitter'] === '') {
      this.error_message = 'Twitter required!';
      this.loading = false
      return;
    } else if(this.data_edit['instagram'] == null || this.data_edit['instagram'] === '') {
      this.error_message = 'Instagram required!';
      this.loading = false
      return;
    } else if(this.data_edit['linkedin'] == null || this.data_edit['linkedin'] === '') {
      this.error_message = 'Linkedin required!';
      this.loading = false
      return;
    } else if(this.data_edit['currency'] == null || this.data_edit['currency'] === '' || this.data_edit['currency'].length != 3) {
      this.error_message = 'Currency required! length must 3';
      this.loading = false
      return;
    } else if((this.data_edit['outsidecatering'] && this.data_edit['corkage'] == null) || (this.data_edit['outsidecatering'] && this.data_edit['corkage'] === '') || (this.data_edit['outsidecatering'] && this.data_edit['corkage'] <= 0) || (this.data_edit['outsidecatering'] && this.data_edit['corkage'] > 100)) {
      this.error_message = 'Corkage required! min 1 and max 100';
      this.loading = false
      return;
    } else if(this.data_edit['pb1'] == null || this.data_edit['pb1'] === '' || this.data_edit['pb1'] < 0 || this.data_edit['pb1'] > 100) {
      this.error_message = 'PB 1 required! min 0 and max 100';
      this.loading = false
      return;
    } else if(this.data_edit['servicecharge'] == null || this.data_edit['servicecharge'] === '' || this.data_edit['servicecharge'] < 0 || this.data_edit['servicecharge'] > 100) {
      this.error_message = 'Service charge required! min 0 and max 100';
      this.loading = false
      return;
    } else if(this.data_edit['open'] == null || this.data_edit['open'] === '' || this.data_edit['open'] < 0 || this.data_edit['open'] > 23) {
      this.error_message = 'Open required! min 0 and max 23';
      this.loading = false
      return;
    } else if(this.data_edit['close'] == null || this.data_edit['close'] === '' || this.data_edit['close'] <= this.data_edit['open']  || this.data_edit['close'] > 24) {
      var temp_open = +this.data_edit['open']
      temp_open = temp_open + 1
      this.error_message = 'Close required! min ' + temp_open + ' and max 24';
      this.loading = false
      return;
    } 

    var re = /(?:\.([^.]+))?$/;

    var logo_ext = re.exec(this.data_edit['logo'])[1];
    const logo_blob = this.logo_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", logo_blob, {
      headers: {
        "Content-Type": "image/" + logo_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.logo_url = res["data"].uploadfiles_fileurl
        var image_ext = re.exec(this.data_edit['image'])[1];
        const image_blob = this.image_file as Blob;
        return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
          headers: {
            "Content-Type": "image/" + image_ext,
            Authorization: "Bearer " + localStorage.getItem('T')
          }
        }).subscribe(

          (res: any) => {
            this.image_url = res["data"].uploadfiles_fileurl
            var brochure_ext = re.exec(this.data_edit['brochure'])[1];
            const image_blob = this.brochure_file as Blob;
            return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
              headers: {
                "Content-Type": "image/" + brochure_ext,
                Authorization: "Bearer " + localStorage.getItem('T')
              }
            }).subscribe(

              (res: any) => {
                this.brochure_url = res["data"].uploadfiles_fileurl
                let data = {
                  "venues_id": this.id_venue,
                  "venues_name": this.data_edit['name'],
                  "venues_propertytype": this.data_edit['propertytype'],
                  "venues_bisnistype": this.data_edit['bisnistype'],
                  "venues_outsidecatering": this.data_edit['outsidecatering'],
                  "venues_corkage": this.data_edit['corkage'],
                  "venues_pb1": this.data_edit['pb1'],
                  "venues_servicecharge": this.data_edit['servicecharge'],
                  "venues_currency": this.data_edit['currency'],
                  "venues_logo": this.logo_url,
                  "venues_image": this.image_url,
                  "venues_email": this.data_edit['email'],
                  "venues_description": this.data_edit['description'],
                  "venues_country": this.data_edit['country'],
                  "venues_city": this.data_edit['city'],
                  "venues_address1": this.data_edit['address1'],
                  "venues_address2": this.data_edit['address2'],
                  "venues_province": this.data_edit['province'],
                  "venues_postalcode": this.data_edit['postalcode'],
                  "venues_longlat": this.data_edit['longitudelatitude'],
                  "venues_phone": this.data_edit['phone'],
                  "venues_open": this.data_edit['open'],
                  "venues_close": this.data_edit['close'],
                  "venues_fullday": 8,
                  "venues_halfday": 4,
                  "venues_overview": this.data_edit['overview'],
                  "venues_website": this.data_edit['website'],
                  "venues_brochure": this.brochure_url,
                  "venues_facebook": this.data_edit['facebook'],
                  "venues_twitter": this.data_edit['twitter'],
                  "venues_instagram": this.data_edit['instagram'],
                  "venues_googleplus": "",
                  "venues_linkedin": this.data_edit['linkedin'], 
                  "venues_yearbuilt": this.data_edit['yearbuilt'],
                  "venues_lastrenovate": this.data_edit['lastrenovate']
                }
                this.http.post(this.globalService.api_baseurl + '/api/bo/client/venue_edit',
                  data, {
                    headers: {
                      "Content-Type": "application/json",
                      'Authorization': 'Bearer ' + localStorage.getItem('T')
                    }
                  }).subscribe(
            
                    (res: any) => {
                      this.loading = false;
                      this.dismiss();
                      return;
                    },
            
                    _error => {
                      this.loading = false
                      if (_error.error.message) {
                        this.error_message = _error.error.message;
                        return;
                      } else {
                        this.error_message = 'please check your network!';
                        return;
                      }
                    }
                  );
              },

              _error => {
                this.loading = false
                if (_error.error.message) {
                  this.error_message = _error.error.message;
                  return;
                } else {
                  this.error_message = 'please check your network!';
                  return;
                }
              }
            );
          },

          _error => {
            this.loading = false
            if (_error.error.message) {
              this.error_message = _error.error.message;
              return;
            } else {
              this.error_message = 'please check your network!';
              return;
            }
          }
        );
      },

      _error => {
        this.loading = false
        if (_error.error.message) {
          this.error_message = _error.error.message;
          return;
        } else {
          this.error_message = 'please check your network!';
          return;
        }
      }
    );
  }


  delete(){
    this.dialog_action = 'delete';   
  }

  go_delete(){
    this.loading = true;
    let data = {
      "venue_id": this.id_venue
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/client/venue_delete', 
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer '+localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          if (res['message'] == 'Success') {
            this.data_message = res['message'];
            this.dismiss();
          } else {
            this.data_message = res['message'];
          }
          this.loading = false
        },
        _error => {
          if (_error.error.message) {
            this.data_message = _error.error.message;
          } else {
            this.data_message = 'please check your network!';
          }
          this.loading = false
        }
      );    
  }

}
