import { Component, OnInit, TemplateRef   } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbMenuService, NbDialogService } from '@nebular/theme';

import { GlobalService } from '../../../@core/data/global.service';

import { SetupStyleAddModalComponent } from './add-modal/add-modal.component';
import { SetupStyleDetailModalComponent } from './detail-modal/detail-modal.component';

@Component({
  selector: 'ngx-setup_style',
  styleUrls: ['./setup_style.component.scss'],
  templateUrl: './setup_style.component.html',
})
export class SetupStyleComponent {

  data_start = 1;
  data_end = 10;
  data_total = 10;
  page_current = 1;
  page_total = 1;

  data_setupstyle = [];
  data_message = 'loading'

  constructor(

    private http: HttpClient,
    private dialogService: NbDialogService,
    private menuService: NbMenuService,
    private globalService: GlobalService
  ) { }

  ngOnInit() {
    this.globalService.startLoading();
    this.globalService.location = 'Client - Setup Style'
    this.load_setupstyle();
  }

  load_setupstyle() {
    this.globalService.startLoading()
    this.http.get(this.globalService.api_baseurl + '/api/bo/client/setup_style_list?page='+this.page_current+'&limit=20', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_setupstyle = res['data'];
          this.data_start = res['meta'].current_data_start;
          this.data_end = res['meta'].current_data_end;
          this.data_total = res['meta'].total_data;
          this.page_current = res['meta'].current_page;
          this.page_total = res['meta'].total_page;
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
  }

  load_detail_setupstyle(id) {
    this.dialogService.open(SetupStyleDetailModalComponent, {
      context: {
        id_setupstyle: id
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_setupstyle() }
    );
  }

  load_add_setupstyle() {
    this.dialogService.open(SetupStyleAddModalComponent, {
      context: {
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_setupstyle() }
    );
  }
}
