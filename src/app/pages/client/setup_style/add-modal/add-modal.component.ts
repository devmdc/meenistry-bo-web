import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-add-modal',
  templateUrl: 'add-modal.component.html',
  styleUrls: ['add-modal.component.scss'],
})
export class SetupStyleAddModalComponent implements OnInit {

  // @Input() id_setupstyle;
  loading = true
  error_message = '';
  dialog_action = 'add'
  name;
  image;
  imageURL;
  image_file: File;
  image_url;

  data_message = 'loading';

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<SetupStyleAddModalComponent>
  ) { }

  ngOnInit() {
    this.loading = true
    this.loading = false
  }

  clear() {
    this.name = null;
    this.image = null;
  }

  dismiss() {
    this.ref.close();
  }

  changeImage(event) {
    if (event.target.files.length === 0) return;
    this.image_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imageURL = reader.result;
    }
  }

  add() {
    this.dialog_action = 'add'
    this.error_message = null
    this.loading = true
    if (this.name == null || this.name === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if (this.image == null || this.image === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    }

    var re = /(?:\.([^.]+))?$/;


    var image_ext = re.exec(this.image)[1];
    const image_blob = this.image_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
      headers: {
        "Content-Type": "image/" + image_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.image_url = res["data"].uploadfiles_fileurl
        let data = {
          "name": this.name,
          "image": this.image_url
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/client/setup_style_add',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    );
  }
}
