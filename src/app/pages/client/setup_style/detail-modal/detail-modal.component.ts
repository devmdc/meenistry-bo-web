import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-detail-modal',
  templateUrl: 'detail-modal.component.html',
  styleUrls: ['detail-modal.component.scss'],
})
export class SetupStyleDetailModalComponent implements OnInit {

  loading = true
  error_message = ''
  @Input() id_setupstyle;

  dialog_action;

  name_setupstyle = '';

  data_detail = [];
  data_edit = {};

  logoURL;
  imageURL;
  brochureURL;
  logo_file: File;
  image_file: File;
  brochure_file: File;
  logo_url;
  image_url;
  brochure_url;

  data_message = 'loading'

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<SetupStyleDetailModalComponent>
  ) {
    this.dialog_action = "detail";
  }

  ngOnInit() {
    this.loading = true
    this.load_setupstyle();
  }

  dismiss() {
    this.ref.close();
  }

  load_setupstyle() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/client/setup_style_detail?setup_style_id=' + this.id_setupstyle, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.name_setupstyle = res['data'].name;

          this.data_detail['name'] = res['data'].name;
          this.data_detail['image'] = res['data'].image;

          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.loading = false
      }
    );
  }

  detail() {
    this.dialog_action = 'detail';
    this.load_setupstyle();
  }

  edit() {
    this.data_edit['name'] = this.data_detail['name'];
    this.imageURL = this.data_detail['image'];

    this.dialog_action = 'edit';
  }

  changeImage(event) {
    if (event.target.files.length === 0) return;
    this.image_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imageURL = reader.result;
    }
  }

  go_revert(){
    this.loading = true
    this.load_setupstyle();
    this.edit();
    this.loading = false
  }

  go_edit() {
    this.dialog_action = 'edit'
    this.error_message = null
    this.loading = true
    if (this.data_edit['name'] == null || this.data_edit['name'] === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if ((this.data_edit['image'] == null && (this.imageURL == null || this.imageURL === '')) || this.data_edit['image'] === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    }

    var re = /(?:\.([^.]+))?$/;  
    var image_ext = re.exec(this.data_edit['image'])[1];
    const image_blob = this.image_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
      headers: {
        "Content-Type": "image/" + image_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.image_url = res["data"].uploadfiles_fileurl
        let data = {
          "setup_style_id": this.id_setupstyle,
          "name": this.data_edit['name'],
          "image": this.image_url
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/client/setup_style_edit',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    );
  }


  delete() {
    this.dialog_action = 'delete';
  }

  go_delete() {
    this.loading = true;
    let data = {
      "setup_style_id": this.id_setupstyle
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/client/setup_style_delete',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          if (res['message'] == 'Success') {
            this.data_message = res['message'];
            this.dismiss();
          } else {
            this.data_message = res['message'];
          }
          this.loading = false
        },
        _error => {
          if (_error.error.message) {
            this.data_message = _error.error.message;
          } else {
            this.data_message = 'please check your network!';
          }
          this.loading = false
        }
      );
  }

}
