import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-add-modal',
  templateUrl: 'add-modal.component.html',
  styleUrls: ['add-modal.component.scss'],
})
export class EventTypeAddModalComponent implements OnInit {

  // @Input() id_setupstyle;
  loading = true
  error_message = '';
  dialog_action = 'add'
  name;
  image;
  imageURL;
  image_file: File;
  image_url;

  data_message = 'loading';

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<EventTypeAddModalComponent>
  ) { }

  ngOnInit() {
    this.loading = true
    this.loading = false
  }

  clear() {
    this.name = null;
    this.image = null;
  }

  dismiss() {
    this.ref.close();
  }

  add() {
    this.dialog_action = 'add'
    this.error_message = null
    this.loading = true
    if (this.name == null || this.name === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    }
    let data = {
      "name": this.name
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/client/event_type_add',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }

      }
    ).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_message = res['message'];
          this.dismiss();
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },

      _error => {
        this.loading = false
        if (_error.error.message) {
          this.error_message = _error.error.message;
          return;
        } else {
          this.error_message = 'please check your network!';
          return;
        }
      }
    );
  }
}
