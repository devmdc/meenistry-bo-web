import { Component, OnInit, TemplateRef   } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbMenuService, NbDialogService } from '@nebular/theme';

import { GlobalService } from '../../../@core/data/global.service';

import { EventTypeAddModalComponent } from './add-modal/add-modal.component';
import { EventTypeDetailModalComponent } from './detail-modal/detail-modal.component';

@Component({
  selector: 'ngx-event_type',
  styleUrls: ['./event_type.component.scss'],
  templateUrl: './event_type.component.html',
})
export class EventTypeComponent {

  data_start = 1;
  data_end = 10;
  data_total = 10;
  page_current = 1;
  page_total = 1;

  data_eventtype = [];
  data_message = 'loading'

  constructor(

    private http: HttpClient,
    private dialogService: NbDialogService,
    private menuService: NbMenuService,
    private globalService: GlobalService
  ) { }

  ngOnInit() {
    this.globalService.startLoading();
    this.globalService.location = 'Client - Event Type'
    this.load_eventtype();
  }

  load_eventtype() {
    this.globalService.startLoading()
    this.http.get(this.globalService.api_baseurl + '/api/bo/client/event_type_list?page='+this.page_current+'&limit=20', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_eventtype = res['data'];
          this.data_start = res['meta'].current_data_start;
          this.data_end = res['meta'].current_data_end;
          this.data_total = res['meta'].total_data;
          this.page_current = res['meta'].current_page;
          this.page_total = res['meta'].total_page;
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
  }

  load_detail_eventtype(id) {
    this.dialogService.open(EventTypeDetailModalComponent, {
      context: {
        id_eventtype: id
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_eventtype() }
    );
  }

  load_add_eventtype() {
    this.dialogService.open(EventTypeAddModalComponent, {
      context: {
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_eventtype() }
    );
  }
}
