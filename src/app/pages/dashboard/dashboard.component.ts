import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GlobalService } from '../../@core/data/global.service';
@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  
  constructor(
    
    private router: Router,
    private globalService: GlobalService
  ) { }

  ngOnInit() {
    this.globalService.startLoading()
    this.globalService.location = 'Dashboard'
    this.globalService.stopLoading()
  }
}
