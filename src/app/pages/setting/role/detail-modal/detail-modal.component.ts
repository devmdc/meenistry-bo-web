import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-detail-modal',
  templateUrl: 'detail-modal.component.html',
  styleUrls: ['detail-modal.component.scss'],
})
export class RoleDetailModalComponent implements OnInit {

  loading = true
  error_message = ''
  @Input() id_role;

  list;
  edit_list;


  dialog_action;

  name_role = '';

  data_detail = [];
  data_edit = {};

  data_message = 'loading'

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<RoleDetailModalComponent>
  ) {
    this.dialog_action = "detail";
  }

  ngOnInit() {
    this.loading = true
    this.load_role();
  }

  dismiss() {
    this.ref.close();
  }

  load_role() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/setting/role_detail?role_id=' + this.id_role, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.name_role = res['data'].name;

          this.data_detail['name'] = res['data'].name;
          this.data_detail['department_name'] = res['data'].department_name;
          this.data_detail['department_id'] = res['data'].department_id;
          this.data_detail['roles_data'] = res['data'].roles_data;
          this.list = res['data'].roles_data;
          this.data_detail['level'] = res['data'].level;
          this.data_detail['sla'] = res['data'].sla;

          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.loading = false
      }
    );
  }

  detail() {
    this.dialog_action = 'detail';
    this.load_role();
  }

  edit() {
    this.data_edit['name'] = this.data_detail['name'];
    this.data_edit['department_id'] = this.data_detail['department_id'];
    this.data_edit['department_name'] = this.data_detail['department_name'];
    this.data_edit['roles_data'] = this.data_detail['roles_data'];
    this.edit_list = this.list;
    this.data_edit['sla'] = this.data_detail['sla'];
    this.data_edit['level'] = this.data_detail['level'];
    this.dialog_action = 'edit';
  }

  go_revert() {
    this.loading = true
    this.load_role();
    this.edit();
    this.loading = false
  }

  go_edit_role(data) {
    if (data) {
      for (let role of data) {
        let data = {
          "id": role["id"],
          "role_id": this.id_role,
          "role_name": this.data_edit['name'],
          "template_id": role["template_id"],
          "show": role["show"]
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/setting/role_edit_detail',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              if (role["data"]) {
                for (let access of role["data"]) {
                  let data = {
                    "id": access["id"],
                    "role_id": this.id_role,
                    "role_name": this.data_edit['name'],
                    "template_id": access["template_id"],
                    "access": access["access"]
                  }
                  this.http.post(this.globalService.api_baseurl + '/api/bo/setting/role_edit_detail_data',
                    data, {
                      headers: {
                        "Content-Type": "application/json",
                        'Authorization': 'Bearer ' + localStorage.getItem('T')
                      }
                    }).subscribe(
              
                      (res: any) => {
                        
                      },
              
                      _error => {
                        this.loading = false
                        if (_error.error.message) {
                          this.error_message = _error.error.message;
                          return;
                        } else {
                          this.error_message = 'please check your network!';
                          return;
                        }
                      }
                    );
                }
              }
              if (role["children"]) {
                this.go_edit_role(role["children"]);
              }
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    }
  }

  go_edit() {
    this.dialog_action = 'edit'
    this.error_message = null
    this.loading = true
    if (this.data_edit['name'] == null || this.data_edit['name'] === '') {
      this.error_message = 'Role name required!';
      this.loading = false
      return;
    } else if (this.data_edit['level'] == null || this.data_edit['level'] === '') {
      this.error_message = 'Role level required!';
      this.loading = false
      return;
    } else if (this.data_edit['sla'] == null || this.data_edit['sla'] === '') {
      this.error_message = 'SLA duration required!';
      this.loading = false
      return;
    }

    let data = {
      "role_id": this.id_role,
      "name": this.data_edit['name'],
      "level": +this.data_edit['level'],
      "sla": +this.data_edit['sla']
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/setting/role_edit',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          this.go_edit_role(this.edit_list);
          this.loading = false;
          this.dismiss();
          return;
        },

        _error => {
          this.loading = false
          if (_error.error.message) {
            this.error_message = _error.error.message;
            return;
          } else {
            this.error_message = 'please check your network!';
            return;
          }
        }
      );
  }


  delete() {
    this.dialog_action = 'delete';
  }

  go_delete() {
    this.loading = true;
    let data = {
      "role_id": this.id_role
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/setting/role_delete',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          if (res['message'] == 'Success') {
            this.data_message = res['message'];
            this.dismiss();
          } else {
            this.data_message = res['message'];
          }
          this.loading = false
        },
        _error => {
          if (_error.error.message) {
            this.data_message = _error.error.message;
          } else {
            this.data_message = 'please check your network!';
          }
          this.loading = false
        }
      );
  }

}
