import { Component, OnInit, TemplateRef   } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbMenuService, NbDialogService } from '@nebular/theme';

import { GlobalService } from '../../../@core/data/global.service';

import { RoleAddModalComponent } from './add-modal/add-modal.component';
import { RoleDetailModalComponent } from './detail-modal/detail-modal.component';

@Component({
  selector: 'ngx-role',
  styleUrls: ['./role.component.scss'],
  templateUrl: './role.component.html',
})
export class RoleComponent {

  data_start = 1;
  data_end = 10;
  data_total = 10;
  page_current = 1;
  page_total = 1;

  data_role = [];
  data_message = 'loading'

  constructor(

    private http: HttpClient,
    private dialogService: NbDialogService,
    private menuService: NbMenuService,
    private globalService: GlobalService
  ) { }

  ngOnInit() {
    this.globalService.startLoading();
    this.globalService.location = 'Setting - Role'
    this.load_role();
  }

  load_role() {
    this.globalService.startLoading()
    this.http.get(this.globalService.api_baseurl + '/api/bo/setting/role_list?page='+this.page_current+'&limit=20', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_role = res['data'];
          this.data_start = res['meta'].current_data_start;
          this.data_end = res['meta'].current_data_end;
          this.data_total = res['meta'].total_data;
          this.page_current = res['meta'].current_page;
          this.page_total = res['meta'].total_page;
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
  }

  load_detail_role(id) {
    this.dialogService.open(RoleDetailModalComponent, {
      context: {
        id_role: id
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_role() }
    );
  }

  load_add_role() {
    this.dialogService.open(RoleAddModalComponent, {
      context: {
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_role() }
    );
  }
}
