import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-add-modal',
  templateUrl: 'add-modal.component.html',
  styleUrls: ['add-modal.component.scss'],
})
export class RoleAddModalComponent implements OnInit {

  // @Input() id_setupstyle;
  loading = true
  error_message = '';
  dialog_action = 'add'
  name;
  department;
  level;
  sla;
  data_department;

  data_message = 'loading';

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<RoleAddModalComponent>
  ) { }

  ngOnInit() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/setting/role_list_department', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_department = res['data'];
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
    this.loading = false
  }

  clear() {
    this.name = null;
    this.department = null;
    this.level = null;
    this.sla = null;
  }

  dismiss() {
    this.ref.close();
  }

  add() {
    this.dialog_action = 'add'
    this.error_message = null
    this.loading = true
    if (this.department == null || this.department === '') {
      this.error_message = 'Department required!';
      this.loading = false
      return;
    } else if (this.name == null || this.name === '') {
      this.error_message = 'Role name required!';
      this.loading = false
      return;
    } else if (this.level == null || this.level === '') {
      this.error_message = 'Role level required!';
      this.loading = false
      return;
    } else if (this.sla == null || this.sla === '') {
      this.error_message = 'SLA duration required!';
      this.loading = false
      return;
    }

        let data = {
          "department_id": +this.department,
          "name": this.name,
          "level": +this.level,
          "sla": +this.sla
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/setting/role_add',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
}
