import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-detail-modal',
  templateUrl: 'detail-modal.component.html',
  styleUrls: ['detail-modal.component.scss'],
})
export class BlogCategoryDetailModalComponent implements OnInit {

  loading = true
  error_message = ''
  @Input() id_blogcategory;

  dialog_action;

  name_blogcategory = '';

  data_detail = [];
  data_edit = {};

  iconURL;
  icon_file: File;
  icon_url;

  data_message = 'loading'

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<BlogCategoryDetailModalComponent>
  ) {
    this.dialog_action = "detail";
  }

  ngOnInit() {
    this.loading = true
    this.load_blogcategory();
  }

  dismiss() {
    this.ref.close();
  }

  load_blogcategory() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/setting/blog_category_detail?blog_category_id=' + this.id_blogcategory, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.name_blogcategory = res['data'].name;

          this.data_detail['name'] = res['data'].name;
          this.data_detail['icon'] = res['data'].icon;
          this.data_detail['status'] = res['data'].status;

          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.loading = false
      }
    );
  }

  detail() {
    this.dialog_action = 'detail';
    this.load_blogcategory();
  }

  edit() {
    this.data_edit['name'] = this.data_detail['name'];
    this.data_edit['status'] = this.data_detail['status'];
    this.iconURL = this.data_detail['icon'];

    this.dialog_action = 'edit';
  }

  changeIcon(event) {
    if (event.target.files.length === 0) return;
    this.icon_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.iconURL = reader.result;
    }
  }

  go_revert(){
    this.loading = true
    this.load_blogcategory();
    this.edit();
    this.loading = false
  }

  go_edit() {
    this.dialog_action = 'edit'
    this.error_message = null
    this.loading = true
    if (this.data_edit['name'] == null || this.data_edit['name'] === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if ((this.data_edit['icon'] == null && (this.iconURL == null || this.iconURL === '')) || this.data_edit['icon'] === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    }

    var re = /(?:\.([^.]+))?$/;  
    var icon_ext = re.exec(this.data_edit['icon'])[1];
    const icon_blob = this.icon_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", icon_blob, {
      headers: {
        "Content-Type": "icon/" + icon_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.icon_url = res["data"].uploadfiles_fileurl
        let data = {
          "blog_category_id": this.id_blogcategory,
          "name": this.data_edit['name'],
          "icon": this.icon_url,
          "status": this.data_edit['status']
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/setting/blog_category_edit',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    );
  }


  delete() {
    this.dialog_action = 'delete';
  }

  go_delete() {
    this.loading = true;
    let data = {
      "blog_category_id": this.id_blogcategory
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/setting/blog_category_delete',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          if (res['message'] == 'Success') {
            this.data_message = res['message'];
            this.dismiss();
          } else {
            this.data_message = res['message'];
          }
          this.loading = false
        },
        _error => {
          if (_error.error.message) {
            this.data_message = _error.error.message;
          } else {
            this.data_message = 'please check your network!';
          }
          this.loading = false
        }
      );
  }

}
