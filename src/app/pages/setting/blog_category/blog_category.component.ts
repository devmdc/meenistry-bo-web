import { Component, OnInit, TemplateRef   } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbMenuService, NbDialogService } from '@nebular/theme';

import { GlobalService } from '../../../@core/data/global.service';

import { BlogCategoryAddModalComponent } from './add-modal/add-modal.component';
import { BlogCategoryDetailModalComponent } from './detail-modal/detail-modal.component';

@Component({
  selector: 'ngx-blog_category',
  styleUrls: ['./blog_category.component.scss'],
  templateUrl: './blog_category.component.html',
})
export class BlogCategoryComponent {

  data_start = 1;
  data_end = 10;
  data_total = 10;
  page_current = 1;
  page_total = 1;

  data_blogcategory = [];
  data_message = 'loading'

  constructor(

    private http: HttpClient,
    private dialogService: NbDialogService,
    private menuService: NbMenuService,
    private globalService: GlobalService
  ) { }

  ngOnInit() {
    this.globalService.startLoading();
    this.globalService.location = 'Setting - Blog Category'
    this.load_blogcategory();
  }

  load_blogcategory() {
    this.globalService.startLoading()
    this.http.get(this.globalService.api_baseurl + '/api/bo/setting/blog_category_list?page='+this.page_current+'&limit=20', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_blogcategory = res['data'];
          this.data_start = res['meta'].current_data_start;
          this.data_end = res['meta'].current_data_end;
          this.data_total = res['meta'].total_data;
          this.page_current = res['meta'].current_page;
          this.page_total = res['meta'].total_page;
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
  }

  load_detail_blogcategory(id) {
    this.dialogService.open(BlogCategoryDetailModalComponent, {
      context: {
        id_blogcategory: id
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_blogcategory() }
    );
  }

  load_add_blogcategory() {
    this.dialogService.open(BlogCategoryAddModalComponent, {
      context: {
      }, closeOnBackdropClick : false, closeOnEsc : false, hasScroll : false
    }).onClose.subscribe(
      (res:any) => { this.load_blogcategory() }
    );
  }
}
