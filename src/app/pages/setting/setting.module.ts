import { NgModule } from '@angular/core';
import { NbDialogModule, NbWindowModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { SettingRoutingModule, routedComponents } from './setting-routing.module';
import { RoleAddModalComponent } from './role/add-modal/add-modal.component';
import { RoleDetailModalComponent } from './role/detail-modal/detail-modal.component';
import { BlogCategoryAddModalComponent } from './blog_category/add-modal/add-modal.component';
import { BlogCategoryDetailModalComponent } from './blog_category/detail-modal/detail-modal.component';

const COMPONENTS = [
  RoleAddModalComponent,
  RoleDetailModalComponent,
  BlogCategoryAddModalComponent,
  BlogCategoryDetailModalComponent
];
const ENTRY_COMPONENTS = [
  RoleAddModalComponent,
  RoleDetailModalComponent,
  BlogCategoryAddModalComponent, 
  BlogCategoryDetailModalComponent
];
const MODULES = [
  ThemeModule,
  NbDialogModule.forChild(),
  NbWindowModule.forChild(),
]; 

@NgModule({
  imports: [
    SettingRoutingModule,
    ...MODULES,
  ],
  declarations: [
    ...routedComponents,
    ...COMPONENTS,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class SettingModule { }
