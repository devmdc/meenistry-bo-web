import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingComponent } from './setting.component';
import { RoleComponent } from './role/role.component';
import { BlogCategoryComponent } from './blog_category/blog_category.component';

const routes: Routes = [{
  path: '',
  component: SettingComponent,
  children: [{
    path: 'role',
    component: RoleComponent,
  },{
    path: 'blog_category',
    component: BlogCategoryComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingRoutingModule { }

export const routedComponents = [
  SettingComponent,
  RoleComponent,
  BlogCategoryComponent,
];
