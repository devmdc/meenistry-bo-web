import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { GlobalService } from '../@core/data/global.service';
import { NbMenuService, NbMenuItem } from '@nebular/theme';
// import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>    
      <nb-menu *ngIf="menu_data_ok" [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {

  menu : NbMenuItem[] = [];
  menu_data : NbMenuItem[] = [];
  menu_data_ok : boolean = false;
  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    private nbMenuService: NbMenuService
  ) {
    if (localStorage.getItem('TF')) {
      this.http.get(this.globalService.api_baseurl + '/api/bo/account_profile?fcm_token=' + localStorage.getItem('TF'), {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(
        (res: any) => {
          if (res["message"] == "Success") {
            this.menu_data = res['data'].roles_data;
            this.nbMenuService.addItems(this.menu_data);
            this.menu_data_ok = true;
          }
        },
        _error => {
          this.globalService.goToLogin();
        }
      );
    } else {
      this.globalService.goToLogin();
    }
  }
}
