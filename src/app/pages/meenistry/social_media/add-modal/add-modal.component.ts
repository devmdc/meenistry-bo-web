import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
@Component({
  selector: 'ngx-add-modal',
  templateUrl: 'add-modal.component.html',
  styleUrls: ['add-modal.component.scss'],
})
export class SocialMediaAddModalComponent implements OnInit {

  // @Input() id_setupstyle;
  loading = true
  error_message = '';
  dialog_action = 'add'
  name;
  url;
  status;
  icon;
  iconURL;
  icon_file: File;
  icon_url;

  data_message = 'loading';

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected ref: NbDialogRef<SocialMediaAddModalComponent>
  ) { }

  ngOnInit() {
    this.loading = true
    this.loading = false
  }

  clear() {
    this.name = null;
    this.icon = null;
    this.url = null;
    this.status = null;
  }

  dismiss() {
    this.ref.close();
  }

  changeIcon(event) {
    if (event.target.files.length === 0) return;
    this.icon_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.iconURL = reader.result;
    }
  }

  add() {
    this.dialog_action = 'add'
    this.error_message = null
    this.loading = true
    if (this.name == null || this.name === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if (this.url == null || this.url === '') {
      this.error_message = 'Url required!';
      this.loading = false
      return;
    } else if (this.icon == null || this.icon === '') {
      this.error_message = 'Icon required!';
      this.loading = false
      return;
    } else if (this.status == null || this.status === '') {
      this.error_message = 'Status required!';
      this.loading = false
      return;
    }
    var re = /(?:\.([^.]+))?$/;


    var icon_ext = re.exec(this.icon)[1];
    const icon_blob = this.icon_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", icon_blob, {
      headers: {
        "Content-Type": "icon/" + icon_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.icon_url = res["data"].uploadfiles_fileurl
        let data = {
          "name": this.name,
          "icon": this.icon_url,
          "url": this.url,
          "status": this.status
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/meenistry/social_media_add',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    );
  }
}
