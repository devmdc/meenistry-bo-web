import { Component } from '@angular/core';

@Component({
  selector: 'ngx-meenistry',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MeenistryComponent {
}
