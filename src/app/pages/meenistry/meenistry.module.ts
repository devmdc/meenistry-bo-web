import { NgModule } from '@angular/core';
import { NbDialogModule, NbWindowModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';

import { EditorModule } from '@tinymce/tinymce-angular';
// import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { MeenistryRoutingModule, routedComponents } from './meenistry-routing.module';
import { SocialMediaAddModalComponent } from './social_media/add-modal/add-modal.component';
import { SocialMediaDetailModalComponent } from './social_media/detail-modal/detail-modal.component';
import { BlogAddModalComponent } from './blog/add-modal/add-modal.component';
import { BlogDetailModalComponent } from './blog/detail-modal/detail-modal.component';

const COMPONENTS = [
  SocialMediaAddModalComponent,
  SocialMediaDetailModalComponent,
  BlogAddModalComponent,
  BlogDetailModalComponent
];
const ENTRY_COMPONENTS = [
  SocialMediaAddModalComponent,
  SocialMediaDetailModalComponent,
  BlogAddModalComponent,
  BlogDetailModalComponent
];
const MODULES = [
  // CKEditorModule,
  EditorModule,
  ThemeModule,
  MeenistryRoutingModule,
  NbDialogModule.forChild(),
  NbWindowModule.forChild(),
];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...routedComponents,
    ...COMPONENTS,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class MeenistryModule { }
