import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeenistryComponent } from './meenistry.component';
import { SocialMediaComponent } from './social_media/social_media.component';
import { BlogComponent } from './blog/blog.component';

const routes: Routes = [{
  path: '',
  component: MeenistryComponent,
  children: [{
    path: 'social_media',
    component: SocialMediaComponent,
  },
  {
    path: 'blog',
    component: BlogComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeenistryRoutingModule { }

export const routedComponents = [
  MeenistryComponent,
  SocialMediaComponent,
  BlogComponent,
];
