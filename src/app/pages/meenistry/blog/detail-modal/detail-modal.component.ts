import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef, NbDateService } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'ngx-detail-modal',
  templateUrl: 'detail-modal.component.html',
  styleUrls: ['detail-modal.component.scss'],
})
export class BlogDetailModalComponent implements OnInit {
  // public Editor = ClassicEditor;

  loading = true
  error_message = ''
  @Input() id_blog;

  data_category;
  dialog_action;

  title_blog = '';

  data_detail = [];
  data_edit = {};

  publish_date;

  data_tag_detail = new Array();
  data_tag = new Array();
  tag;
  
  min: Date;

  imageURL;
  image_file: File;
  image_url;

  data_message = 'loading'

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected dateService: NbDateService<Date>,
    protected ref: NbDialogRef<BlogDetailModalComponent>
  ) {
    this.dialog_action = "detail";
    this.min = this.dateService.addDay(this.dateService.today(), -1);
  }

  ngOnInit() {
    this.loading = true
    this.load_blog();
  }

  dismiss() {
    this.ref.close();
  }

  load_blog() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/meenistry/blog_detail?blog_id=' + this.id_blog, {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ' + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.title_blog = res['data'].title;

          this.data_detail['title'] = res['data'].title;
          this.data_detail['image'] = res['data'].image;
          this.data_detail['category_id'] = res['data'].category_id;
          this.data_detail['category_name'] = res['data'].category_name;
          this.data_detail['striped_body'] = res['data'].striped_body;
          this.data_detail['html_body'] = res['data'].html_body;
          this.data_detail['publish_at'] = new Date(res['data'].publish_at);
          this.data_detail['status'] = res['data'].status;
          this.data_detail['highlight'] = res['data'].highlight;
          for (let tag_data of res['data'].tags) {
            this.data_tag_detail.push(tag_data.tag)
          }
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.loading = false
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.loading = false
      }
    );
  }

  detail() {
    this.dialog_action = 'detail';
    this.load_blog();
  }

  add_tag(tag){
    var cek = false;
    for (let tag_data of this.data_tag) {
      if (tag_data == tag) {
        cek = true;
      }
    }
    if (cek == false) {
      this.data_tag.push(tag.toUpperCase())
    }
    this.tag = null;
  }

  delete_tag(tag){
    var data_tag_temp = this.data_tag;
    this.data_tag = new Array();
    for (let tag_data of data_tag_temp) {
      if (tag_data != tag) {
        this.data_tag.push(tag_data)        
      }
    }
  }

  edit() {
    this.http.get(this.globalService.api_baseurl + '/api/bo/meenistry/blog_list_category', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_category = res['data'];
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
    this.data_edit['title'] = this.data_detail['title'];
    this.data_edit['category_id'] = this.data_detail['category_id'];
    this.data_edit['category_name'] = this.data_detail['category_name'];
    this.data_edit['striped_body'] = this.data_detail['striped_body'];
    this.data_edit['html_body'] = this.data_detail['html_body'];
    this.data_edit['publish_at'] = this.data_detail['publish_at'];
    this.data_edit['status'] = this.data_detail['status'];
    this.data_edit['highlight'] = this.data_detail['highlight'];
    this.imageURL = this.data_detail['image'];
    this.data_tag = this.data_tag_detail;
    this.publish_date = new Date(this.data_detail['publish_at']);

    this.dialog_action = 'edit';
  }

  changeImage(event) {
    if (event.target.files.length === 0) return;
    this.image_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imageURL = reader.result;
    }
  }

  go_revert(){
    this.loading = true
    this.load_blog();
    this.edit();
    this.loading = false
  }

  go_edit() {
    this.dialog_action = 'edit'
    this.error_message = null
    this.loading = true
    if (this.data_edit['title'] == null || this.data_edit['title'] === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if ((this.data_edit['image'] == null && (this.imageURL == null || this.imageURL === '')) || this.data_edit['image'] === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    }

    var re = /(?:\.([^.]+))?$/;  
    var image_ext = re.exec(this.data_edit['image'])[1];
    const image_blob = this.image_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
      headers: {
        "Content-Type": "image/" + image_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.image_url = res["data"].uploadfiles_fileurl
        let data = {
          "blog_id": this.id_blog,
          "cateegory_id": +this.data_edit["category_id"],
          "image": this.image_url,
          "title": this.data_edit["title"],
          "html_body": this.data_edit["html_body"],
          "striped_body": this.data_edit["striped_body"],
          "status": this.data_edit["status"],
          "publish_at": this.publish_date,
          "highlight": false,
          "tags": []
        }
        for (let tag_data of this.data_tag) {
          data.tags.push({"tag":tag_data});
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/meenistry/blog_edit',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    );
  }


  delete() {
    this.dialog_action = 'delete';
  }

  go_delete() {
    this.loading = true;
    let data = {
      "blog_id": this.id_blog
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/meenistry/blog_delete',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          if (res['message'] == 'Success') {
            this.data_message = res['message'];
            this.dismiss();
          } else {
            this.data_message = res['message'];
          }
          this.loading = false
        },
        _error => {
          if (_error.error.message) {
            this.data_message = _error.error.message;
          } else {
            this.data_message = 'please check your network!';
          }
          this.loading = false
        }
      );
  }

}
