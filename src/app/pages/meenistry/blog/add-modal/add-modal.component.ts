import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbDialogRef, NbDateService } from '@nebular/theme';

import { GlobalService } from '../../../../@core/data/global.service';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'ngx-add-modal',
  templateUrl: 'add-modal.component.html',
  styleUrls: ['add-modal.component.scss'],
})
export class BlogAddModalComponent implements OnInit {
  // public Editor = ClassicEditor;
  // @Input() id_setupstyle;
  loading = true
  error_message = '';
  dialog_action = 'add'

  min: Date;

  highlight = false;
  title;
  striped;
  data_category;
  category;
  status;
  publish_date;
  data_tag = new Array();
  tag;
  image;
  imageURL;
  image_file: File;
  image_url;
  content = '';
  data_message = 'loading';

  constructor(
    private http: HttpClient,
    private globalService: GlobalService,
    protected dateService: NbDateService<Date>,
    protected ref: NbDialogRef<BlogAddModalComponent>
  ) {
    this.min = this.dateService.addDay(this.dateService.today(), -1);
  }

  ngOnInit() {
    this.loading = true
    this.http.get(this.globalService.api_baseurl + '/api/bo/meenistry/blog_list_category', {
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer '+localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        if (res['message'] == 'Success') {
          this.data_category = res['data'];
          this.data_message = res['message'];
        } else {
          this.data_message = res['message'];
        }
        this.globalService.stopLoading()
      },
      _error => {
        if (_error.error.message) {
          this.data_message = _error.error.message;
        } else {
          this.data_message = 'please check your network!';
        }
        this.globalService.stopLoading()
      }
    );
    this.loading = false
  }

  add_tag(tag){
    var cek = false;
    for (let tag_data of this.data_tag) {
      if (tag_data == tag) {
        cek = true;
      }
    }
    if (cek == false) {
      this.data_tag.push(tag.toUpperCase())
    }
    this.tag = null;
  }

  delete_tag(tag){
    var data_tag_temp = this.data_tag;
    this.data_tag = new Array();
    for (let tag_data of data_tag_temp) {
      if (tag_data != tag) {
        this.data_tag.push(tag_data)        
      }
    }
  }


  clear() {
    this.title = null;
    this.striped = null;
    this.category = null;
    this.status = null;
    this.publish_date = null;
    this.tag = null;
    this.data_tag = new Array();
    this.image = null;
    this.image_url = null;
    this.content = '';
  }

  dismiss() {
    this.ref.close();
  }

  changeImage(event) {
    if (event.target.files.length === 0) return;
    this.image_file = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imageURL = reader.result;
    }
  }

  add() {
    this.dialog_action = 'add'
    this.error_message = null
    this.loading = true
    if (this.title == null || this.title === '') {
      this.error_message = 'Name required!';
      this.loading = false
      return;
    } else if (this.striped == null || this.striped === '') {
      this.error_message = 'Striped required!';
      this.loading = false
      return;
    } else if (this.category == null || this.category === '') {
      this.error_message = 'Category required!';
      this.loading = false
      return;
    } else if (this.status == null || this.status === '') {
      this.error_message = 'Status required!';
      this.loading = false
      return;
    } else if (this.publish_date == null || this.publish_date === '') {
      this.error_message = 'Publish Date required!';
      this.loading = false
      return;
    } else if (this.image == null || this.image === '') {
      this.error_message = 'Image required!';
      this.loading = false
      return;
    } else if (this.content == null || this.content === '') {
      this.error_message = 'Content required!';
      this.loading = false
      return;
    }

    var re = /(?:\.([^.]+))?$/;


    var image_ext = re.exec(this.image)[1];
    const image_blob = this.image_file as Blob;
    return this.http.post(this.globalService.api_baseurl + "/api/filesystem/upload", image_blob, {
      headers: {
        "Content-Type": "image/" + image_ext,
        Authorization: "Bearer " + localStorage.getItem('T')
      }
    }).subscribe(

      (res: any) => {
        this.image_url = res["data"].uploadfiles_fileurl
        let data = {
          "cateegory_id": +this.category,
          "image": this.image_url,
          "title": this.title,
          "html_body": this.content,
          "striped_body": this.striped,
          "status": this.status,
          "publish_at": this.publish_date,
          "highlight": this.highlight,
          "tags": []
        }
        for (let tag_data of this.data_tag) {
          data.tags.push({"tag":tag_data});
        }
        this.http.post(this.globalService.api_baseurl + '/api/bo/meenistry/blog_add',
          data, {
            headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer ' + localStorage.getItem('T')
            }
          }).subscribe(

            (res: any) => {
              this.loading = false;
              this.dismiss();
              return;
            },

            _error => {
              this.loading = false
              if (_error.error.message) {
                this.error_message = _error.error.message;
                return;
              } else {
                this.error_message = 'please check your network!';
                return;
              }
            }
          );
      }
    );
  }
}
