import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { UserService } from '../@core/data/users.service';
import { GlobalService } from '../@core/data/global.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private http: HttpClient,
    private router: Router,
    private usersService: UserService,
    private globalService: GlobalService
  ) { }

  message: any = '';

  ngOnInit() {
    localStorage.clear();
    this.globalService.location = 'Login'    
    this.globalService.stopLoading()
  }

  login(event) {
    this.globalService.startLoading()
    event.preventDefault();

    const target = event.target;
    const email = target.querySelector('#email').value;
    const password = target.querySelector('#password').value;

    const fcm_token = 'hardcode_fcm_token' + target.querySelector('#email').value

    this.http.post(this.globalService.api_baseurl + '/api/bo/account_login', {
      headers: {
        "Content-Type": "application/json"
      },
      email: email,
      password: password,
      from: "web",
      fcm_token: fcm_token
    }).subscribe(

      (res: any) => {
        this.usersService.setUser(
          res['data'].users_full_name,
          res['data'].users_token,
          fcm_token
        )
        this.router.navigate(['pages/dashboard']);
        return;
      },

      _error => {
        if (_error.error.message) {
          this.message = _error.error.message;
        } else {
          this.message = 'please check your network!';
        }
        this.globalService.stopLoading()
        return;
      }
    );
  }
}
