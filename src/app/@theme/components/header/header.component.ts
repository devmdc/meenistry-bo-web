import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { NbMenuService, NbSidebarService, NbGlobalLogicalPosition, NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { UserService } from '../../../@core/data/users.service';
import { GlobalService } from '../../../@core/data/global.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;
  index = 1;


  constructor(
    private http: HttpClient,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserService,
    private globalService: GlobalService,
    private analyticsService: AnalyticsService,
    private toastrService: NbToastrService) {
  }

  ngOnInit() {
    this.userService.getUser().subscribe(
      (res: any) => {
        this.user = res
      },
      _error => {
        this.globalService.goToLogin();
      }
    );
    this.globalService.stopLoading();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  logOut() {
    this.globalService.startLoading()
    let data = {
      "from": "web"
    }
    this.http.post(this.globalService.api_baseurl + '/api/bo/account_logout',
      data, {
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer ' + localStorage.getItem('T')
        }
      }).subscribe(

        (res: any) => {
          this.globalService.goToLogin();
          return;
        },

        _error => {
          this.globalService.stopLoading()
          if (_error.error.message) {
            this.showToast(NbToastStatus.DANGER, 'Log Out', _error.error.message)
            return;
          } else {
            this.showToast(NbToastStatus.DANGER, 'Log Out', 'please check your network!')
            return;
          }
        }
      );
  }

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      titleContent,
      config);
  }
}
